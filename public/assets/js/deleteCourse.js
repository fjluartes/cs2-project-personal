console.log("delete course");
let token = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');

if (!token || token === null) {
  alert("You must sign in first.");
  // window.location.replace('./login.html');
  window.location.href = './login.html'; 
} else {
  fetch(`https://aqueous-taiga-75662.herokuapp.com/api/courses/${courseId}`, {
    method: "DELETE",
    headers: {
      "Content-type": "application/json",
      "Authorization": `Bearer ${token}`
    }
  }).then(res => {
    return res.json();
  }).then(data => {
    if (data) {
      Swal.fire({
        icon: "success",
        title: "Success",
        text: "Course successfully deleted",
        confirmButtonText: 'Back to Courses'
      }).then(() => {
        window.location.href="./courses.html";
      });
    }
  });
}
