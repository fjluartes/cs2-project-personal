console.log("Script JS file");
let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let pathname = window.location.pathname;
let page = pathname.split('/').pop();
// console.log(page);

if (!userToken) {
  if (page === "index.html") {
    navItems.innerHTML = `
      <li class="nav-item">
        <a href="./pages/login.html" class="nav-link"> Sign in </a>
      </li>
    `;
  } else {
    navItems.innerHTML = `
      <li class="nav-item">
        <a href="./login.html" class="nav-link"> Sign in </a>
      </li>
    `;
  }
} else {
  if (page === "index.html") {
    navItems.innerHTML = `
      <li class="nav-item">
        <a href="./pages/logout.html" class="nav-link"> Sign out </a>
      </li>
    `;
  } else {
    navItems.innerHTML = `
      <li class="nav-item">
        <a href="./logout.html" class="nav-link"> Sign out </a>
      </li>
    `;
  }
}
