console.log("Profile JS");
let token = localStorage.getItem("token");
let profile = document.querySelector("#profileContainer");

if (!token || token === null) {
  alert("You must sign in first.");
  // window.location.replace('./login.html');
  window.location.href = './login.html'; 
} else {
  // console.log(token);
  // fetch(`http://localhost:4000/api/users/details`, {
  fetch(`https://aqueous-taiga-75662.herokuapp.com/api/users/details`, {
    method: 'GET',
    headers: {
      "Content-type": "application/json",
      "Authorization": `Bearer ${token}`
    }
  }).then(res => {
    return res.json();
  }).then(async (user) => {
    // console.log(user);
    let enrollmentData = user.enrollments;

    // await fetch(`http://localhost:4000/api/courses/`).then(res => {
    await fetch(`https://aqueous-taiga-75662.herokuapp.com/api/courses/`).then(res => {
      return res.json();
    }).then(courses => {
      enrollmentData.forEach(classData => {
        let id = classData.courseId;
        courses.forEach(course => {
          if (id == course._id) {
            classData.courseName = course.name;
          }
        });
      });
    });
    let newEnrollmentData = enrollmentData.map(classData => {
      return (`
          <tr>
            <td>${classData.courseName}</td>
            <td>${classData.enrolledOn}</td>
            <td>${classData.status}</td>
          </tr>`
      );
    });
    
    let profileData = `
     <div class="col-md-12">
       <section class="jumbotron my-5">
         <h3 class="text-center">Name: ${user.firstName} ${user.lastName}</h3>
         <h3 class="text-center">Email: ${user.email}</h3>
         <h3 class="text-center">Mobile: ${user.mobileNo}</h3>
         <table class="table">
           <thead>
             <tr>
               <th>Course ID: </th>
               <th>Enrolled on: </th>
               <th>Status: </th>
               <tbody></tbody>
             </tr>
           </thead>
           <tbody>
           ${newEnrollmentData}
           </tbody>
         </table>
       </section>
     </div>
    `;
    profile.innerHTML = profileData;
  });
}
 
