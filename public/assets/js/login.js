console.log("Login JS");

let loginForm = document.querySelector("#loginUser");

loginForm.addEventListener('submit', (e) => {
  e.preventDefault();
  let email = document.querySelector("#userEmail").value;
  let password = document.querySelector("#password").value;

  if (email === "" || password === "") {
    Swal.fire({
      icon: "warning",
      title: "Oops!",
      text: "Please input your email and/or password"
    });
  } else {
    // fetch("http://localhost:4000/api/users/login", {
    fetch("https://aqueous-taiga-75662.herokuapp.com/api/users/login", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email,
        password
      })
    }).then(res => {
      return res.json();
    }).then(data => {
      // console.log(data.access);
      if (data.access) {
        // insert data.access in local storage
        localStorage.setItem('token', data.access);
        // alert("access key saved on local storage");
        // fetch(`http://localhost:4000/api/users/details`, {
        fetch(`https://aqueous-taiga-75662.herokuapp.com/api/users/details`, {
          headers: {
            "Authorization": `Bearer ${data.access}`
          }
        }).then(res => {
          return res.json();
        }).then(data => {
          // console.log(data);
          localStorage.setItem('id', data._id);
          localStorage.setItem('isAdmin', data.isAdmin);
          // alert("items set in local storage");
          window.location.replace('./courses.html');
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Something went wrong. Check your credentials"
        });
      }
    });
  }
});
