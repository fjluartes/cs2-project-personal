let registerUserForm = document.querySelector("#registerUser");

registerUserForm.addEventListener("submit", (e) => {
  e.preventDefault();
  let firstName = document.querySelector("#firstName").value;
  let lastName = document.querySelector("#lastName").value;
  let email = document.querySelector("#userEmail").value;
  let mobileNo = document.querySelector("#mobileNumber").value;
  let password = document.querySelector("#password1").value;
  let verifyPassword = document.querySelector("#password2").value;

  if (password === "" || verifyPassword === "" ||
      password !== verifyPassword || mobileNo.length < 11) {
    Swal.fire({
      icon: "error",
      title: "Oops",
      text: "Finish the form first before clicking register"
    });
  } else {
    // fetch("http://localhost:4000/api/users/email-exists", {
    fetch("https://aqueous-taiga-75662.herokuapp.com/api/users/email-exists", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email
      })
    }).then(res => {
      return res.json();
    }).then(data => {
      if (data === true) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Email already exists"
        });
      } else {
        // fetch("http://localhost:4000/api/users/register", {
        fetch("https://aqueous-taiga-75662.herokuapp.com/api/users/register", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            firstName,
            lastName,
            email, 
            mobileNo,
            password
          })
        }).then(res => {
          return res.json();
        }).then(data => {
          console.log(data);
          if (data === true) {
            Swal.fire({
              icon: "success",
              title: "Success",
              text: "New User Registered Successfully",
              confirmButtonText: 'Proceed to Login',
            }).then(() => {
              window.location.href="./login.html";
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "Error",
              text: "Something went wrong in the registration"
            });
          }
          document.querySelector("#firstName").value = "";
          document.querySelector("#lastName").value = "";
          document.querySelector("#userEmail").value = "";
          document.querySelector("#mobileNumber").value = "";
          document.querySelector("#password1").value = "";
          document.querySelector("#password2").value = "";
          // window.location.href="./login.html";
        });
      }
    });
  }
});
