console.log("Display single course here");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
// console.log(courseId);
let token = localStorage.getItem("token");
// console.log(token);

let name = document.querySelector("#courseName");
let desc = document.querySelector("#courseDesc");
let price = document.querySelector("#coursePrice");
let enroll = document.querySelector("#enrollmentContainer");

if (!token || token === null) {
  alert("You must sign in first.");
  // window.location.replace('./login.html');
  window.location.href = './login.html'; 
} else {
  // console.log(token);
  // fetch(`http://localhost:4000/api/courses/${courseId}`).then(res => {
  fetch(`https://aqueous-taiga-75662.herokuapp.com/api/courses/${courseId}`).then(res => {
    return res.json();
  }).then(data => {
    // console.log(data);
    if (data.length < 1) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Course does not exist"
      });
    } else {
      name.innerHTML = data.name;
      desc.innerHTML = data.description;
      price.innerHTML = data.price;
      enroll.innerHTML = `<a id="enrollButton" class="btn btn-block btn-primary my-3">Enroll</a>`;
      // enroll.innerHTML = `
      // <form id="enrollButton">
      //   <button type="submit" class="btn btn-block btn-primary my-3">Enroll</button>
      // </form>
      // `;

      // addEventListener for enroll button
      let enrollButton = document.querySelector("#enrollButton");
      // enrollButton.addEventListener("submit", (e) => {
      enrollButton.addEventListener("click", (e) => {
        e.preventDefault();
        // fetch(`http://localhost:4000/api/users/enroll`, {
        fetch(`https://aqueous-taiga-75662.herokuapp.com/api/users/enroll`, {
          method: 'POST',
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
          },
          body: JSON.stringify({
            courseId
          })
        }).then(res => {
          return res.json();
        }).then(data => {
          // console.log(data);
          if (data) {
            Swal.fire({
              icon: "success",
              title: "Success",
              text: "Course successfully enrolled",
              confirmButtonText: 'Back to Courses'
            }).then(() => {
                window.location.href="./courses.html";
            });
          }
        });
      });
    }
  });
}


