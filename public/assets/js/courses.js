console.log("Courses JS");
let modalButton = document.querySelector("#adminButton");
let container = document.querySelector("#coursesContainer");
let isAdmin = localStorage.getItem("isAdmin");
// console.log(isAdmin);
let cardFooter;
let token = localStorage.getItem("token");

if (!token || token === null) {
  alert("You must sign in first");
  // window.location.replace('./login.html');
  window.location.href = './login.html'; 
} else {
  // console.log(token);
  if (isAdmin == "false" || !isAdmin) {
    modalButton.innerHTML = null;
  } else {
    modalButton.innerHTML = `
    <div class="col-md-2 offset-md-10">
      <a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
    </div>
  `;
  }  
  // fetch('http://localhost:4000/api/courses/').then(res => {
  fetch('https://aqueous-taiga-75662.herokuapp.com/api/courses/').then(res => {
    return res.json();
  }).then(data => {
    // console.log(data);
    let courseData;
    if (data.length < 1) {
      courseData = "No Courses Available";
    } else {
      courseData = data.map(course => {
        // console.log(course._id);
        
        if (isAdmin == "false" || !isAdmin) {
          cardFooter = `
          <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">
            View Course Details
          </a>
        `;
        } else {
          cardFooter = `
          <a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">
            Edit Course
          </a>
          <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">
            Delete Course
          </a>
        `;
        }
        return (`
          <div class="col-md-6 my-3">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">₱${course.price}</p>
					<p class="card-text text-left">${course.description}</p>
				</div>
			</div>
            <div class="card-footer">${cardFooter}</div>
		  </div>
        `);
      }).join("");
      // join concatenates objects inside the array
      container.innerHTML = courseData;
    }
  });  
}

