let createCourse = document.querySelector("#createCourse");
let token = localStorage.getItem("token");

if (!token || token === null) {
  alert("You must sign in first");
  // window.location.replace('./login.html');
  window.location.href = './login.html'; 
} else {
  // console.log(token);
  createCourse.addEventListener("submit", (e) => {
    e.preventDefault();
    let courseName = document.querySelector("#courseName").value;
    let coursePrice = document.querySelector("#coursePrice").value;
    let courseDescription = document.querySelector("#courseDescription").value;
    
    if (courseName === "" || coursePrice === "" || courseDescription === "") {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Something went wrong. Check your input."
      });
    } else {
      // check if course exists
      // fetch("http://localhost:4000/api/courses/course-exists", {
      fetch("https://aqueous-taiga-75662.herokuapp.com/api/courses/course-exists", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          courseName
        })
      }).then(res => {
        return res.json();
      }).then(data => {
        // console.log(data);
        if (data === true) {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: "Course already exists"
          });
        } else {
          // fetch("http://localhost:4000/api/courses/addCourse", {
          fetch("https://aqueous-taiga-75662.herokuapp.com/api/courses/addCourse", {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              courseName,
              courseDescription,
              coursePrice
            })
          }).then(res => {
            return res.json();
          }).then(data => {
            console.log(data);
            if (data === true) {
              Swal.fire({
                icon: "success",
                title: "Success",
                text: "New Course Added Successfully"
              });
            } else {
              Swal.fire({
                icon: "error",
                title: "Error",
                text: "Something went wrong when adding the course"
              });
            }
            document.querySelector("#courseName").value = "";
            document.querySelector("#coursePrice").value = "";
            document.querySelector("#courseDescription").value = "";
          });
        }
      });
    }
  });
}

